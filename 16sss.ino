/* Project Name: 16sss
 * 
 * Description:
 *   16 step simple sequencer
 */

// Configurable parameters, adjust it to your build

#define MIN_TEMPO            60        
#define MAX_TEMPO            360
#define NUM_STEPS_MAX        16        
#define NUM_VOICES_MAX       5        
#define MIDI_CHANNEL         10       


#define PIXEL_CHIP           WS2812B  
#define MIN_BRIGHT           64       
#define MAX_BRIGHT           255      

#define SWEEP_HUE            0 
#define MARK_HUE             128    

#define NOTE_1               35
#define NOTE_2               40
#define NOTE_3               41
#define NOTE_4               46 
#define NOTE_5               44

#define CHANNEL              10

// Pin map, modify pins to set your build
#define PIN_B_1             2
#define PIN_B_2             4
#define PIN_B_3             5
#define PIN_B_4             6
#define PIN_B_5             7
#define PIN_PAUSE           9
#define PIN_PIXELS_CLK     10   
#define PIN_PIXELS         12


#define PIN_SAVE            20
#define PIN_LOAD            21
#define PIN_RESET           22        


#define PIN_TEMPO          A0 
#define PIN_NUM_STEPS      A1
#define PIN_POSITION       A2
#define PIN_BRIGHTNESS     A3


const uint8_t pin_buttonVoice[NUM_VOICES_MAX]= {PIN_B_1,PIN_B_2,PIN_B_3,PIN_B_4,PIN_B_5}; // Change this detebding of number of vioces and pinout

// Danger area. No more configuration beyond these line
//---------------------------------------------------------------------------------------------------------------------------------------

uint16_t  tempo = 60;
uint32_t antes = 0;
uint8_t  nStep = 0;
uint8_t  numSteps = NUM_STEPS_MAX;
uint8_t  pos = 0;
boolean  pause = false;

boolean sequence[NUM_VOICES_MAX][NUM_STEPS_MAX] = {};
const uint8_t notesChannel[NUM_VOICES_MAX] = {NOTE_1,NOTE_2,NOTE_3,NOTE_2,NOTE_5};
#include "Bounce2.h"

Bounce resetButton = Bounce();
Bounce pauseButton = Bounce();
Bounce buttonVoice[NUM_VOICES_MAX];
Bounce buttonSave = Bounce();
Bounce buttonLoad = Bounce();

const uint8_t debounceTime = 5; // Milliseconds


#include "FastLED.h"

const uint16_t NUM_LEDS=NUM_VOICES_MAX*NUM_STEPS_MAX;
CRGB ledMatrix[NUM_LEDS];
uint8_t  bright=32;

#include "EEPROM.h"




void setup() {
  
  pinMode(PIN_RESET,INPUT_PULLUP);
  pinMode(PIN_PAUSE,INPUT_PULLUP);
  pinMode(PIN_SAVE,INPUT_PULLUP);
  pinMode(PIN_LOAD,INPUT_PULLUP);
  
  resetButton.attach(PIN_RESET);
  pauseButton.attach(PIN_PAUSE);
  buttonSave.attach(PIN_SAVE);
  buttonLoad.attach(PIN_LOAD);
  
  resetButton.interval(debounceTime);
  pauseButton.interval(debounceTime);
  buttonSave.interval(debounceTime);
  buttonLoad.interval(debounceTime);

  
  for (uint8_t v=0 ; v < NUM_VOICES_MAX; v++) {
    for (uint8_t s=0; s<NUM_STEPS_MAX; s++) {
      sequence[v][s]=0;
    } 
  }

  for (uint8_t b=0 ; b < NUM_VOICES_MAX; b++) {
      buttonVoice[b] = Bounce();
      pinMode(pin_buttonVoice[b],INPUT_PULLUP);
      buttonVoice[b].attach(pin_buttonVoice[b]);
      buttonVoice[b].interval(debounceTime);
  }
  LEDS.addLeds<PIXEL_CHIP,PIN_PIXELS,BRG>(ledMatrix,NUM_LEDS);
  FastLED.setDither( 0 );
}

void loop() {
  readInput();
  manageButtonChannels();

  if (buttonSave.rose()) {
    saveSeq();
  }

  if (buttonLoad.rose()) {
    pause=true;
    loadSeq();
    pause=false;
  } 

  if (pauseButton.rose()) {
    pause = !pause;
  }

  if (resetButton.rose()) {
    Serial.println("Reset button");
    pause=true;
    resetSeq();
    pause=false;
  }

  elapsedMillis ahora = millis();
  uint32_t periodo = (60*1000)/tempo;
  animMatrix();
  if (!pause && (ahora - antes > periodo)) {   
    antes=ahora;
    
    execStep(nStep);
    debugStep();
    
    nStep++; 
    if (nStep > numSteps) {
      nStep=0;
    }   
  }
   
    FastLED.setBrightness(64);
    FastLED.show();
    delay(1);
}

void readInput() {
  // Read Potentiometers
  tempo = map(analogRead(PIN_TEMPO),0,1023,MIN_TEMPO,MAX_TEMPO);
  numSteps = map(analogRead(PIN_NUM_STEPS),0,1023,0,NUM_STEPS_MAX-1);
  pos=map(analogRead(PIN_POSITION),0,1023,0,NUM_STEPS_MAX-1);
//  bright=map(analogRead(PIN_NUM_STEPS),0,1023,0,NUM_STEPS_MAX);

  //Update button status
  pauseButton.update();
  for (uint8_t i=0; i > NUM_VOICES_MAX; i++) {
    buttonVoice[i].update();
  }

  buttonSave.update();
  buttonLoad.update();
  resetButton.update();
}

void execStep(uint8_t s) {
  for (uint8_t i = 0 ; i < NUM_VOICES_MAX; i++) {
    if (sequence[i][nStep]!= 0) {
      usbMIDI.sendNoteOn(notesChannel[i],127,10);     
    } else {
      usbMIDI.sendNoteOff(notesChannel[i],127,10);
    }
  }
}


void apagarTodo() { for(int i = 0; i < NUM_LEDS; i++) { ledMatrix[i]=CRGB(0,0,0); } }

void debugStep() {
  Serial.print("Paso "); Serial.print(nStep);
  Serial.print("/");Serial.print(numSteps); Serial.print(" "); Serial.print(tempo); Serial.print(" BPM");
  Serial.print(" "); Serial.print("(");Serial.print(pos);Serial.print(") ");
  for (uint8_t i=0; i< NUM_VOICES_MAX; i++) {
    Serial.print(sequence[i][nStep]); Serial.print(" ");
  }
  Serial.println();
}


void manageButtonChannels() {
  for (uint8_t i=0; i<NUM_VOICES_MAX; i++) {
    buttonVoice[i].update();
    if (buttonVoice[i].fell()){
      if (sequence[i][pos]==0) {
        sequence[i][pos]=1;
      } else {
        sequence[i][pos]=0;
      }    
    }
  }
}


void updateNotesMatrix() {
  for (uint8_t v=0; v < NUM_VOICES_MAX; v++) {
    for (uint8_t p = 0; p < NUM_STEPS_MAX; p++) {
      if (sequence[v][p]) {
        ledMatrix[p+(v*NUM_STEPS_MAX)]=CHSV((255/NUM_VOICES_MAX)*v,255,255);
      }
    }
  ledMatrix[(nStep)+(16*v)]=CHSV(SWEEP_HUE,255,128);
  }
}


void animMatrix() {
  apagarTodo();
  updateNotesMatrix();
  markPosition();
}

void markPosition() {
  for (uint8_t v = 0; v < NUM_VOICES_MAX; v ++) {
    ledMatrix[pos+(NUM_STEPS_MAX*v)]+=CRGB(12,12,12);
  }
}

void resetSeq()  {
  Serial.println("Reset inicio");
  for (uint8_t v=0 ; v < NUM_VOICES_MAX; v++) {
    for (uint8_t s=0; s<NUM_STEPS_MAX; s++) {
      sequence[v][s]=0;
    } 
  }
  Serial.println("Reset fin");
}


void saveSeq() {
  uint32_t address = pos*NUM_STEPS_MAX*NUM_VOICES_MAX;
  Serial.print("Save in bank ");Serial.print(pos); Serial.print(" address "); Serial.println(address);
  for (uint8_t v = 0; v< NUM_VOICES_MAX; v++) {
    for (uint8_t p = 0; p< NUM_STEPS_MAX; p++) {
      Serial.print("Writting in ");Serial.print(v); Serial.print(","); Serial.println(p);
      EEPROM.update(address,sequence[v][p]);
      address++;
     }
  }
}

void loadSeq() {
  resetSeq();
  uint32_t address = pos*NUM_STEPS_MAX*NUM_VOICES_MAX;
  Serial.print("Save in bank ");Serial.print(pos); Serial.print(" address "); Serial.println(address);
  for (uint8_t v = 0; v< NUM_VOICES_MAX; v++) {
    for (uint8_t p = 0; p< NUM_STEPS_MAX; p++) {
      Serial.print("Loading from ");Serial.print(v); Serial.print(","); Serial.println(p);
      sequence[v][p] = EEPROM.read(address);
      address++;
     }
  }
}
